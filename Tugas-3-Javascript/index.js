// SOAL 1
var pertama = "saya sangat senang hari ini"
var kedua = "belajar javascript itu keren"

var saya = pertama.substr(0,4);
var senang = pertama.substr(12,6);
var belajar = kedua.substr(0,7);
var javascript = kedua.substr(8,10);
javascript = javascript.toUpperCase();

console.log(saya+' '+senang+' '+belajar+' '+javascript);

// SOAL 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var Pertama =Number(kataPertama);
var Kedua =Number(kataKedua);
var Ketiga =Number(kataKetiga);
var Keempat =Number(kataKeempat);

var hasil = Pertama + Keempat + (Kedua * Ketiga);
console.log(hasil);

// SOAL 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substring(25,31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
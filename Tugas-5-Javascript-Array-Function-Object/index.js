// SOAL 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
daftarHewan.forEach(function(e){
    console.log(e);
})

// SOAL 2
function introduce(name,age,address,hobby){
    var me = {};
    me.name = name;
    me.age = age;
    me.address = address;
    me.hobby = hobby;
    var kenalan = 'nama saya '+ [name] +' Umur saya '+[age]+', alamat saya di '+[address]+', dan saya punya hobby '+[hobby];
    return kenalan;
}
var perkenalan = introduce('Fahmi',21,'Bandung','Futsal');
console.log(perkenalan)


// SOAL 3
function hitung_huruf_vokal(str){
    var hitung = str.match(/[aeiou]/gi).length;
    return hitung;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

// SOAL 4
function hitung(angka){
    hasil = (angka * 2) - 2;
    return hasil
}
console.log( hitung(0) )
console.log( hitung(1) )
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
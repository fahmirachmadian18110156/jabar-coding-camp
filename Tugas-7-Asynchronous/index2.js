var readBooksPromise = require('./promise.js')

var books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    },
    {
        name: 'komik',
        timeSpent: 1000
    }
]

function baca(remainingTime, books, index) {
    readBooksPromise(remainingTime, books[index], function (time) {
        const nextBook = index + 1;
        if (nextBook < books.length) {
            baca(time, books, nextBook);
        }
    });
}

baca(10000, books, 0);
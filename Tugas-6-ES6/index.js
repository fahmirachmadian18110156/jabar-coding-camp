// SOAL 1
const persegi = (panjang = 2, lebar = 3) => {
    luas = panjang * lebar;
    keliling = 2 * (panjang + lebar);
    console.log(luas);
    console.log(keliling);
}

persegi();

// SOAL 2
const newFunction = (firstName, lastName) => {
    firstName = 'William';
    lastName = 'Imoh';
    console.log(`${firstName} ${lastName}`);
}

//Driver Code 
newFunction()

// SOAL 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {
    firstName,
    lastName,
    address,
    hobby
} = newObject
console.log(firstName, lastName, address, hobby)

// SOAL 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined  = [...west, ...east]
//Driver Code
console.log(combined)

// SOAL 5
const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
const before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet}`
